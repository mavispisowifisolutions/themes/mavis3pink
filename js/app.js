(function($, window){
  var alerts = function(message, type) {
    if (!type) {type = 'alert-success'}
    $.notify({ message: message }, {
      type: type,
      allow_dismiss: true,
      newest_on_top: true,
      timer: 1000,
      placement: {
          from: 'top',
          align: 'left'
      },
      animate: {
          enter: 'animated fadeInDown',
          exit: 'animated fadeOutUp'
      },
      z_index: 999999999,
      template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} p-r-35" role="alert">' +
      '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
      '<span data-notify="icon"></span> ' +
      '<span data-notify="title">{1}</span> ' +
      '<span data-notify="message">{2}</span>' +
      '<div class="progress" data-notify="progressbar">' +
      '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
      '</div>' +
      '<a href="{3}" target="{4}" data-notify="url"></a>' +
      '</div>'
    });
  }
	$( function() {
    var pisowifi = new Pisowifi();
    var my_distance = false;
    var my_turn = false;
    
    var coindrop = new Howl({
      src: [audio.coindrop.webm, audio.coindrop.mp3]
    });
    var addtime = new Howl({
      src: [audio.addtime.webm, audio.addtime.mp3]
    });
    
    var confirm_btn_check = function(){
      $('.btn-insert-coin').prop('disabled',!(my_distance && my_turn))
      .toggleClass('btn-secondary',!(my_distance && my_turn)).toggleClass('btn-success',(my_distance && my_turn));
    }
    
    pisowifi.on('wifi', function( data ){
      var connected = data.connected === true;
      $('.wifi .switch :checkbox').prop( { 'checked': connected } );
      $('.wifi .text').text( connected?'Internet':'No internet');
      $('.wifi').toggleClass( 'internet', connected );
    })
    
    pisowifi.on('timer', function( time, status ){
      var _time = time({raw:true}) > 0;
      $('#connection').prop('disabled',!_time)
      $('.time').toggleClass('active', status > 0);
      $('.time .number, .modal .timer').text( time() );
    })
    
    pisowifi.on('heartbeat', function( data ){
      my_distance = ( data.distance.max == 0 ) || ( data.distance.max > data.distance.current );
      my_turn = data.myturn === true;
      var s1 = 'Press Insert Coin before inserting coin.';
      var s2 = 'Go to Coinbox...'
      if ( my_distance ) {
        if ( data.myturn === true ) {
          s1 = 'Press Insert Coin before inserting coin.';
        } else {
          if ( data.myturn ) {
            s1 = 'Wait for your turn... ('+data.myturn+')';
          } else {
            s1 = 'Wait for your turn...';
          }
        }
      } else {
        s1 = 'You are far away. Go near the coinbox';
      }
      
      $('.coinslot .text-priority').text(s1);
      confirm_btn_check();
    })
    
    pisowifi.on('insertcoin-timer', function( data ){
      var seconds = data.time > 1? data.time + ' seconds':data.time + ' second';
      $('.wait-timer,.progress.insert-coin').css({visibility:'visible'})
      $('#insertcoin .seconds-timer').text(seconds)
      $('.progress.insert-coin .progress-bar').width(((data.time/data.max)*100) + '%');
      if (data.time <= 1) {
        $('#insertcoin').modal('hide')
      }
      if (data['coindrop']) {
        coindrop.play();
      }
      if (data['added-time']) {
        coindrop.stop();
        addtime.play();
        alerts("Success: Added <strong>" + ( data['added-time'] / 60 ) + "</strong> minutes to your time.")
      }
    })
    
    pisowifi.on('submit-voucher', function( data ){
      var type = data.result == 'error'? 'danger': data.result;
      data.result = data.result.charAt(0).toUpperCase() + data.result.slice(1);
      var voucher = 'Server Error';
      if (type == 'success') {
        if ( data.status == 0 ) {
          voucher = "Time: +<strong>" + (data.time.toHHMMSS()) + "</strong>";
        } else {
          type = 'danger';
          data.result = 'Error'
          voucher = "Used Voucher";
        }
        $('.voucher-code').val('');
      }
      alerts(data.result + "<br/>" + voucher, "alert-" + type);
    })
    
    $(document).on('change click', '.wifi .switch :checkbox', function(e){
      e.preventDefault();
      e.stopPropagation();
      return false;
    });
    $(document).on('input', '.voucher-code', function(){
      $('.btn-voucher').prop('disabled', $.trim(this.value).length <= 4 )
    });
    $(document).on('click', '.btn-voucher', function(){
      var data = { 
        event: 'submit-voucher',
        code: $.trim($('.voucher-code').val())
      };
      var btn = $(this).prop('disabled', true ).data('text', $(this).text())
      var counter = 3;
      var tt;
      clearInterval(tt);
      tt = setInterval(function(){
        btn.text(function(){
          return $(this).data('text') + ' (' + ( counter-- ) + ')';
        });
        btn.prop('disabled', ( counter >= 0 ) || ($.trim($('.voucher-code').val()).length <= 4) )
        if (counter < 0) {
          clearInterval(tt);
          btn.text(btn.data('text'))
        }
      },1000)
      pisowifi.trigger( 'plugin.events', [data] );
    });
    $(document).on('click','.btn-insert-coin', function(e){
      setTimeout(function() {
        $('#insertcoin').modal('show');
      }, relayDelay );
      $(this).text('Please wait...')
      return false;
    })
    
    $('#insertcoin').on('show.bs.modal', function (e) {
      var data = {
        request: true,
        event: 'insertcoin'
      }
      $('.wait-timer,.progress.insert-coin').css({visibility:'hidden'})
      pisowifi.trigger( 'plugin.events', [data] );
    })
    $('#insertcoin').on('hide.bs.modal', function (e) {
      var data = {
        request: false,
        event: 'insertcoin'
      }
      pisowifi.trigger( 'plugin.events', [data] );
      $('.progress.insert-coin .progress-bar').width('100%');
      setTimeout(function () {
        window.location.reload(true);
      },1500)
    })
    
    $('#mainslide').on('slid.bs.carousel', function () {
      var meta = $('.header-color');
      var color = $(this).find('.active').data('color');
      meta.attr('content', color);
    });
    $('.table-rates tbody').each(function () {
      var rates = {};
      try {
        rates = options['universal-coinslot']['cointable'];
      } catch(e){}
      if (!rates) {return;}
      var tbody = $(this);
      tbody.empty();
      var template = '<tr><th class="rts" scope="row">{{credit}} php</th><td>{{time}}</td></tr>';
      
      $.each(rates, function (key, rate) {
        var tmp = template.replace('{{credit}}', rate.credits);
        tmp = tmp.replace('{{time}}', rate.time.toHHMMSS());
        tbody.append(tmp)
      })
    });
	});
})( jQuery, window );